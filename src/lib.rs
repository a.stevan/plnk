#![doc = include_str!("../README.md")]

use std::time::{Duration, Instant};

use indicatif::{ProgressBar, ProgressStyle};

/// hold information about the benchmark to run
pub struct Bencher {
    /// the number of times each bench should run, the higher this number the lower the variance
    nb_measurements: usize,
    /// the name of the benchmark
    name: String,
}

impl Bencher {
    pub fn new(nb_measurements: usize) -> Self {
        Self {
            nb_measurements,
            name: "".to_string(),
        }
    }

    /// add a name to a bencher
    pub fn with_name(&self, name: impl ToString) -> Self {
        Self {
            nb_measurements: self.nb_measurements,
            name: name.to_string(),
        }
    }
}

/// run a given piece of code a bunch of times and output the measurements
///
/// - label: an additional label to differentiate similar but different things to benchmark in the
/// same bencher
/// - f: the piece of code to run and measure
/// - the measurements will be printed to STDOUT as JSON
pub fn bench<F>(b: &Bencher, label: &str, mut f: F)
where
    F: FnMut() -> Duration,
{
    eprintln!("bencher: {}, label: {}", b.name, label,);
    let pb = ProgressBar::new(b.nb_measurements as u64)
        .with_style(ProgressStyle::default_bar().progress_chars("#*-"));

    let mut times = vec![];
    for _ in 0..b.nb_measurements {
        times.push(f().as_nanos());

        pb.inc(1);
    }

    println!(
        r#"{{"label": "{}", "name": "{}", "times": {:?}}}"#,
        label, b.name, times
    );
}

/// measure the time it takes to do something
///
/// # Example
/// ```rust
/// # use plnk::timeit;
/// let t = timeit(|| 1 + 2);
/// ```
pub fn timeit<F, O>(mut f: F) -> Duration
where
    F: FnMut() -> O,
{
    let start_time = Instant::now();
    let _ = f();
    Instant::now().duration_since(start_time)
}
