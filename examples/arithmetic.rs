use clap::Parser;
use rand::Rng;

fn arithmetic(b: &plnk::Bencher) {
    plnk::bench(b, "addition", || plnk::timeit(|| 1 + 2));
    plnk::bench(b, "substraction", || plnk::timeit(|| 1 - 2));
    plnk::bench(b, "multiplication", || plnk::timeit(|| 2 * 3));
}

fn random(b: &plnk::Bencher) {
    let mut rng = rand::thread_rng();
    plnk::bench(b, "sampling", || plnk::timeit(|| rng.gen::<u128>()));
}

#[derive(Parser, Debug)]
struct Args {
    #[arg(short, long, default_value_t = 10)]
    nb_measurements: usize,
}

fn main() {
    let args = Args::parse();

    let bencher = plnk::Bencher::new(args.nb_measurements);

    arithmetic(&bencher.with_name("arithmetic"));
    random(&bencher.with_name("random"));
}
