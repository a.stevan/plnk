use clap::Parser;

fn capture(b: &plnk::Bencher) {
    let outer = 10;
    plnk::bench(b, "dummy", || {
        let inner = outer;
        plnk::timeit(|| inner + outer)
    });
}

#[derive(Parser, Debug)]
struct Args {
    #[arg(short, long, default_value_t = 10)]
    nb_measurements: usize,
}

fn main() {
    let args = Args::parse();

    let bencher = plnk::Bencher::new(args.nb_measurements);

    capture(&bencher.with_name("capture"));
}
