# PLNK - a simple benchmark framework

## installation
```shell
cargo add plnk --git https://gitlab.isae-supaero.fr/a.stevan/plnk              # latest
cargo add plnk --git https://gitlab.isae-supaero.fr/a.stevan/plnk --tag 0.2.0  # a specific version
```

## usage
see the [arithmetic example](`examples/arithmetic.rs`) which can be used as
```shell
cargo run --release --example arithmetic -- --nb-measurements 1000
```

## the output of PLNK
the output of the snippet above will have two parts:

### on STDERR
the benchmarking process will show progress bars while running and the details
of each benchmark will be shown as follows
```text
bencher: arithmetic, label: addition
bencher: arithmetic, label: substraction
bencher: arithmetic, label: multiplication
bencher: random, label: sampling
```

### on STDOUT
the measurements as valid NDJSON, i.e. one JSON object per line
```json
{"label": "addition", "name": "arithmetic", "times": [69, 14, 15, 14, 13, 13, 15, 14, 15, 14]}
{"label": "substraction", "name": "arithmetic", "times": [29, 14, 14, 15, 15, 14, 13, 14, 13, 15]}
{"label": "multiplication", "name": "arithmetic", "times": [28, 13, 15, 14, 14, 14, 13, 15, 14, 14]}
{"label": "sampling", "name": "random", "times": [35675, 86, 63, 74, 53, 75, 74, 54, 53, 87]}
```
