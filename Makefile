.PHONY: fmt check clippy test
DEFAULT: check clippy test

check:
	cargo fmt --all -- --check
	cargo check --workspace --all-targets

fmt:
	cargo fmt --all

clippy:
	cargo clippy --workspace --all-targets -- -D warnings

test:
	cargo test --workspace
